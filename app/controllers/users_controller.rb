class UsersController < ApplicationController
  def new; end

  def create
    user = User.new(user_params)
    if user.save
      session[:user_id] = user.id
      account = ChartOfAccounts.new_account_for user
      redirect_to account_path(account.id)
    else
      redirect_to new_user_path
    end
  end

  private

  def user_params
    params.require(:user)
          .permit(
            :name,
            :email,
            :password,
            :password_confirmation
          )
  end
end
