class AccountsController < ApplicationController
  before_action :authorize

  rescue_from InvalidAccountError, with: :not_found

  def show
    @account = ChartOfAccounts.load(params[:id])
  end

  def balance
    @account = ChartOfAccounts.load(params[:id])
  end

  def transfer
    @another_accounts = load_accounts_to_transfer
    @account = ChartOfAccounts.load(params[:id])
  end

  def create_transfer
    Transfer.perform(params[:id], params[:destination_id], params[:amount])
    flash[:success] = 'Transfer completed!'
    redirect_to account_path
  rescue InvalidAccountError, IllegalOperationError, InsuficientFundsError => e
    flash[:error] = e.message
    redirect_to transfer_path
  end

  private

  def load_accounts_to_transfer
    ChartOfAccounts.other_than(params[:id]).map do |account|
      [ account.user_name, account.id ]
    end
  end
end
