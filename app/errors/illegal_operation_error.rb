class IllegalOperationError < RuntimeError
  def initialize
    super('You cannot use negative amounts for transactions')
  end
end
