class InvalidAccountError < RuntimeError
  def initialize
    super('Invalid account given')
  end
end
