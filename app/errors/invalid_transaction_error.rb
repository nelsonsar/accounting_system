class InvalidTransactionError < RuntimeError
  def initialize
    super('Transaction was created in a inapropriated way')
  end
end
