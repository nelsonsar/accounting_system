class InsuficientFundsError < RuntimeError
  def initialize
    super('Account does not have enough balance to complete the operation')
  end
end
