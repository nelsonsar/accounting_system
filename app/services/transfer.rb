class Transfer
  class << self
    def perform(source_id, destination_id, amount)
      amount = BigDecimal.new(amount.to_s)
      from = ChartOfAccounts.load source_id
      to = ChartOfAccounts.load destination_id

      raise InsuficientFundsError.new unless has_sufficient_funds? from, amount

      transaction = Transaction.schedule(from, to, amount)

      transaction.commit description(source_id, destination_id, amount)
    end

    private

    def description(source_id, destination_id, amount)
      format(
        'Transferred %s from %s to %s'.freeze,
        BigDecimal.new(amount.to_s),
        source_id,
        destination_id
      )
    end

    def has_sufficient_funds?(account, amount)
      account.balance - amount > 0
    end
  end
end
