# A service to create an account for a user replacing
# Rails callbacks
class ChartOfAccounts
  def self.new_account_for(user)
    Account.create!(user: user)
  end

  def self.load(id)
    begin
      Account.find id
    rescue ActiveRecord::RecordNotFound
      raise InvalidAccountError.new
    end
  end

  def self.other_than(id)
    Account.where.not(id: id)
  end
end
