class Transaction < ApplicationRecord
  belongs_to :debit_entry, class_name: Entry
  belongs_to :credit_entry, class_name: Entry

  class << self
    def schedule(left_account, right_account, amount)
      raise InvalidAccountError.new unless left_account && right_account
      raise IllegalOperationError.new unless amount > 0

      transaction = new
      transaction.build_debit_entry debit_entry(left_account, amount)
      transaction.build_credit_entry credit_entry(right_account, amount)

      transaction
    end

    private

    def credit_entry(account, amount)
      account.credit(amount).as_json
    end

    def debit_entry(account, amount)
      account.debit(amount).as_json
    end
  end


  def commit(memo)
    self.memo = memo
    raise InvalidTransactionError.new unless can_be_commited? && save
    self
  end

  private

  def can_be_commited?
    if debit_entry && credit_entry
      debit_entry.amount + credit_entry.amount == 0
    end
  end
end
