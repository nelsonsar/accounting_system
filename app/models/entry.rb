class Entry < ApplicationRecord
  has_one :account

  validates_presence_of :when
  validates_presence_of :amount

  # Returns a new instance of Entry booked for an account with given amount
  # booked to now
  #
  # This method is a named constructor to make the domain clearer.
  def self.book(account, amount)
    new(
      account_id: account.id,
      when: DateTime.now,
      amount: BigDecimal.new(amount.to_s)
    )
  end
end
