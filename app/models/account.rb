class Account < ApplicationRecord
  belongs_to :user
  has_many :entries

  def credit(amount)
    entries.build entry(amount)
  end

  def debit(amount)
    entries.build entry(-amount)
  end

  def balance
    entries.reduce(0) { |total, entry| total + entry.amount }
  end

  def user_name
    user.name
  end

  private

  # Returns a hash representation of a new book
  #
  # The idea of Account class is to work as a read only model for entries
  # and the only way to add an object to a collection in Rails without
  # persisting it is to using the +collection.build+ method which takes a
  # hash with attributes as parameter.
  def entry(amount)
    Entry.book(self, amount).as_json
  end
end
