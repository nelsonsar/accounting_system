Rails.application.routes.draw do
  resources :users

  # Session handling routes
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  # Account actions

  resources :accounts, only: [:show]
  get 'accounts/:id/balance', to: 'accounts#balance', as: :balance
  get 'accounts/:id/transfers/new', to: 'accounts#transfer', as: :transfer
  post 'accounts/:id/transfers', to: 'accounts#create_transfer', as: :transfers

  root to: 'sessions#new'
end
