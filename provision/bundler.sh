function install_bundler {
    sudo apt-get install nodejs -y
    (cd /magnetis; gem install bundler; bundle install)
}
