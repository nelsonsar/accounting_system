# Bank accounting system

A simple bank accouting system based on Martin Fowler's Accounting Patterns

## Getting started

### Dependencies

* Ruby 2.4.0
* Rails 5.0.1
* PostgreSQL 9.3+ or SQLite

### Setup the project

1. Clone the project
2. Enter the project folder
3. Install gems
4. Create the database

### Running the project

* Run Rails server

  `$ rails server`
