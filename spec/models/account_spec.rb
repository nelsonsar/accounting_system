require 'rails_helper'

describe Account do
  let(:account) { described_class.new }

  describe '#balance' do
    it 'shows current account balance' do
      expect(account.balance).to eq 0
    end
  end

  describe '#credit' do
    it 'decreases balance value' do
      expect { account.credit 10 }.to change(account, :balance).by(10)
    end
  end

  describe '#debit' do
    it 'increases balance value' do
      expect { account.debit 10 }.to change(account, :balance).by(-10)
    end
  end

  describe '#user_name' do
    it 'returns associated user name' do
      account.build_user name: 'Foo'

      expect(account.user_name).to eq 'Foo'
    end
  end
end

