require 'rails_helper'

describe Entry do
  describe '.book' do
    it 'creates a new entry for an account with a given amount' do
      entry = Entry.book(Account.new, 10)

      expect(entry.amount).to eq 10
    end
  end
end
