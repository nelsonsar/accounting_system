require 'rails_helper'

describe Transaction do
  describe '.schedule' do
    context 'with valid accounts' do
      it 'returns a new instance of Transaction' do
        transaction = described_class.schedule(
          Account.new,
          Account.new,
          100
        )
        expect(transaction).to be_an_instance_of(Transaction)
      end
    end

    context 'with invalid accounts' do
      it 'raises an InvalidAccountError' do
        expect do
          described_class.schedule(
            Account.new,
            nil,
            100
          )
        end.to raise_error(InvalidAccountError)
      end
    end

    context 'with invalid amount' do
      it 'it raises an IllegalOperationError' do
        expect do
          described_class.schedule(
            Account.new,
            Account.new,
            -100
          )
        end.to raise_error(IllegalOperationError)
      end
    end
  end

  describe '#commit' do
    context 'valid transaction' do
      it 'saves transaction with given description' do
        transaction = described_class.schedule(
          Account.new,
          Account.new,
          100
        )

        transaction.commit('A transfer')

        expect(transaction.id).to_not be_nil
      end
    end

    context 'invalid transaction' do
      it 'raises InvalidTransactionError' do
        transaction = described_class.new

        expect do
          transaction.commit('An invalid operation')
        end.to raise_error(InvalidTransactionError)
      end
    end

    # This test is just a defensive measure because of Rails
    # needing the initialize method to work
    context 'when entries have different values' do
      it 'raises InvalidTransactionError' do
        transaction = described_class.new
        transaction.build_debit_entry Entry.book(Account.new, -100).as_json
        transaction.build_credit_entry Entry.book(Account.new, 200).as_json

        expect do
          transaction.commit('An invalid operation')
        end.to raise_error(InvalidTransactionError)
      end
    end
  end
end
