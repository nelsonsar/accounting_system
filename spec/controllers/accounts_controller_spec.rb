require 'rails_helper'
require 'support/user_factory'

describe AccountsController do
  let(:user) { UserFactory.create }

  describe 'GET #show' do
    context 'anonymous user' do
      it 'redirects user to login page' do
        get :show, params: { id: 1 }

        expect(response).to redirect_to login_path
      end
    end

    context 'authenticated user' do
      it 'renders account information' do
        get :show, session: { user_id: user.id, }, params: { id: user.id }

        expect(response).to render_template :show
      end
    end
  end

  describe 'GET #balance' do
    context 'with valid account id' do
      it 'renders balance template' do
        get :balance, session: { user_id: user.id, },
                      params: { id: user.account.id }

        expect(response).to render_template :balance
      end
    end

    context 'with invalid account id' do
      it 'renders 404 page' do
        get :balance, session: { user_id: user.id, }, params: { id: 9999 }

        expect(response).to redirect_to root_path
        expect(flash[:error]).to_not be_nil
      end
    end
  end

  describe 'GET #transfer' do
    it 'renders transfer form' do
      account = double
      allow(account).to receive(:id).and_return(99)
      allow(account).to receive(:user_name).and_return('John Doe')

      expect(ChartOfAccounts).to receive(:other_than).and_return([account])

      get :transfer, session: { user_id: user.id, },
                     params: { id: user.account.id }

      expect(response).to render_template :transfer
    end
  end

  describe 'POST #create_transfer' do
    context 'with invalid account' do
      it 'redirects to transfer form' do
        expect(Transfer).to receive(:perform).and_raise InvalidAccountError

        post :create_transfer, session: { user_id: user.id, },
                               params: {
                                 id: user.account.id,
                                 destination_id: 999,
                                 amount: 100
                               }

        expect(response).to redirect_to transfer_path
        expect(flash[:error]).to_not be_nil
      end
    end

    context 'with insuficient funds' do
      it 'redirects to transfer form' do
        expect(Transfer).to receive(:perform).and_raise InsuficientFundsError

        post :create_transfer, session: { user_id: user.id, },
                               params: {
                                 id: user.account.id,
                                 destination_id: 2,
                                 amount: 100
                               }

        expect(response).to redirect_to transfer_path
        expect(flash[:error]).to_not be_nil
      end
    end

    context 'with invalid amount' do
      it 'redirects to transfer form' do
        expect(Transfer).to receive(:perform).and_raise IllegalOperationError

        post :create_transfer, session: { user_id: user.id, },
                               params: {
                                 id: user.account.id,
                                 destination_id: 2,
                                 amount: -100
                               }

        expect(response).to redirect_to transfer_path
        expect(flash[:error]).to_not be_nil
      end
    end

    context 'with valid parameters' do
      it 'redirects user to account path' do
        expect(Transfer).to receive(:perform)

        post :create_transfer, session: { user_id: user.id, },
                               params: {
                                 id: user.account.id,
                                 destination_id: 2,
                                 amount: 100
                               }

        expect(response).to redirect_to account_path(user.account.id)
        expect(flash[:success]).to_not be_nil
      end
    end
  end
end
