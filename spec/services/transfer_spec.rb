require 'rails_helper'
require 'support/account_factory'

describe Transfer do
  describe '.perform' do
    context 'when invalid accounts given' do
      it 'raises InvalidAccountError' do
        source_id = 98
        destination_id = 99
        amount = 100

        expect do
          Transfer.perform(source_id, destination_id, amount)
        end.to raise_error(InvalidAccountError)
      end
    end

    context 'when source account has insuficient funds' do
      it 'raises InsuficientFundsError' do
        source = AccountFactory.with_no_movements
        destination = AccountFactory.with_no_movements

        expect do
          Transfer.perform(source.id, destination.id, '100')
        end.to raise_error(InsuficientFundsError)
      end
    end

    context 'with valid information' do
      it 'saves transaction' do
        source = AccountFactory.with_positive_balance
        destination = AccountFactory.with_no_movements

        Transfer.perform(source.id, destination.id, '100')

        expect(source.balance).to eq 900
        expect(destination.balance).to eq 100
      end
    end
  end
end
