require 'rails_helper'
require 'support/user_factory'

describe ChartOfAccounts do
  describe '.new_account_for' do
    it 'creates a new account for given user' do
      result = described_class.new_account_for UserFactory.create

      expect(result).to be_an_instance_of(Account)
    end
  end

  describe '.load' do
    context 'existing account' do
      let(:account) { AccountFactory.with_no_movements }
      it 'it returns the account with given id' do
        result = described_class.load(account.id)

        expect(result).to be_an_instance_of(Account)
      end
    end

    context 'inexistent user' do
      it 'raises InvalidAccountError' do
        expect { described_class.load(999) }.to raise_error InvalidAccountError
      end
    end
  end

  describe '.other_than' do
    it 'returns all accounts that do not match given id' do
      account = AccountFactory.with_no_movements
      AccountFactory.with_positive_balance

      expect(described_class.other_than(account.id)).to_not include(account)
    end
  end
end
