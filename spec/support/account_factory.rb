require_relative 'user_factory'

module AccountFactory
  def self.with_no_movements
    user = UserFactory.create

    user.account
  end

  def self.with_positive_balance
    account = with_no_movements

    Entry.book(account, 1_000).save

    account
  end
end
