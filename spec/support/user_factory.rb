module UserFactory
  def self.create
    user = User.create!(name: 'Foo', email: 'foo@foo.com', password: '12345')
    Account.create!(user: user)

    user
  end
end
