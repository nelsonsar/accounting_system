class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.string :memo
      t.references :debit_entry, index: true, foreign_key: { to_table: :entries }
      t.references :credit_entry, index: true, foreign_key: { to_table: :entries }
      t.timestamps
    end
  end
end
