class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.references :account, foreign_key: true
      t.datetime :when, null: false
      t.decimal :amount, precision: 8, scale: 2, null: false
    end
  end
end
